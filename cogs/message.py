from discord.ext import commands

class Message(commands.Cog):
	def __init__(self, bot):
		self.bot = bot # sets the client variable so we can use it in cogs
  
	@commands.Cog.listener()
	async def on_ready(self):
		print('Ready!')
		print('Logged in as ---->', self.bot.user)
		print('ID:', self.bot.user.id)

	@commands.command()
	async def hello(self, ctx):
		await ctx.send("hello world !!")
  
	@commands.Cog.listener(name = "on_message")
	async def ping(self, message):
		if message.author == self.bot.user:
			return

		if message.content == "ping":
			print("in")
			await message.channel.send("pong")


async def setup(bot):
    await bot.add_cog(Message(bot))