import asyncio
import os

import discord
from discord.ext import commands
from dotenv import load_dotenv

# ---------------------------------------------------------------------------- #
#                                   PRE INIT                                   #
# ---------------------------------------------------------------------------- #

load_dotenv()  # load environment variables from .env.

# # ---------------------------------------------------------------------------- #
# #                                    CONFIG                                    #
# # ---------------------------------------------------------------------------- #

BOT_API_KEY = os.getenv("BOT_API_KEY")

intents = discord.Intents.default()
intents.members = True
intents.message_content = True

# ---------------------------------------------------------------------------- #

class Application(commands.Bot):
    _bot_starting_message = "[INFO]: Bot is now online"
    _bot_status = "Bot still online"
    
    _message_channel = 1040163874789392414
    
    def __init__(self, command_prefix, self_bot, intents):
        commands.Bot.__init__(self, command_prefix = command_prefix, self_bot = self_bot, intents = intents)
        self.add_commands()
    
    async def on_ready(self):
        channel = bot.get_channel(Application._message_channel)
        print(Application._bot_starting_message)
        await channel.send(Application._bot_starting_message)
    
    async def load_extensions(self):
        for filename in os.listdir("./cogs"):
            if filename.endswith(".py"):
                await self.load_extension(f"cogs.{filename[:-3]}")
    
    def add_commands(self):
        @self.command(name="status", pass_context=True)
        async def status(ctx):
            await ctx.channel.send(Application._bot_status + " " + ctx.author.mention)
        
# ---------------------------------------------------------------------------- #

bot = Application(command_prefix="!", self_bot=False, intents=intents)

async def main():
    async with bot:
        await bot.load_extensions()
        await bot.start(BOT_API_KEY)

asyncio.run(main())

# ---------------------------------------------------------------------------- #